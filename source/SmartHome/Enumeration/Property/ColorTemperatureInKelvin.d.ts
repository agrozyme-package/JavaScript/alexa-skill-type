/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#colortemperatureinkelvin */

// @formatter:off
declare const enum ColorTemperatureInKelvin {
  warm = 2200,
  incandescent = 2700,
  white = 4000,
  daylight = 5500,
  cool = 7000,
}
// @formatter:on

export default ColorTemperatureInKelvin;
