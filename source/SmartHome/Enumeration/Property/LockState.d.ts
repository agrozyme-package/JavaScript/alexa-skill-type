/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#lockstate */

// @formatter:off
declare const enum LockState {
  LOCKED = 'LOCKED',
  UNLOCKED = 'UNLOCKED',
  JAMMED = 'JAMMED',
}
// @formatter:on

export default LockState;
