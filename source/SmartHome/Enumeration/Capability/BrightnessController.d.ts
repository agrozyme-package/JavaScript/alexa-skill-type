// @formatter:off

export const enum BrightnessControllerPayloadName {
  brightnessDelta = 'brightnessDelta',
  brightness = 'brightness',
}

export const enum BrightnessControllerRequestName {
  AdjustBrightness = 'AdjustBrightness',
  SetBrightness = 'SetBrightness',
}

export const enum BrightnessControllerPropertyName {
  brightness = 'brightness'
}

// @formatter:on
