/** @link https://developer.amazon.com/docs/smapi/interaction-model-schema.html */

import Dialog from './Dialog';
import LanguageModel from './LanguageModel';
import Prompt from './Prompt';

export default interface InteractionModel {
  interactionModel: { dialog?: Dialog; languageModel: LanguageModel; prompts?: Prompt[]; }
}


