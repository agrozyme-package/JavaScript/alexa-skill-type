import Endpoint from './Endpoint';
import Header from './Header';
import Property from './Property';

export default interface SkillResponse {
  context?: { properties: Property[]; };
  event: { header: Header; payload: { [index: string]: any }; endpoint?: Endpoint; };
}
