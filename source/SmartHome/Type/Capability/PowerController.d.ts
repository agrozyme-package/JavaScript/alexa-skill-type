import PowerState from '../../Enumeration/Property/PowerState';
import Endpoint from '../Endpoint';
import Header from '../Header';
import Property from '../Property';

export interface PowerControllerRequest {
  directive: { header: Header; endpoint: Endpoint; payload: {}; };
}

export interface PowerControllerProperty extends Property {
  value: PowerState;
}

export interface PowerControllerReponse {
  context: { properties: PowerControllerProperty[]; };
  event: { header: Header; endpoint: Endpoint; payload: {}; };
}
