/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#connectivity */

// @formatter:off
declare const enum Connectivity {
  OK = 'OK',
  UNREACHABLE = 'UNREACHABLE',
}
// @formatter:on

export default Connectivity;
