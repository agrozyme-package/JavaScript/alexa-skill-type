/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#thermostatmode */

// @formatter:off
declare const enum ThermostatMode {
  AUTO = 'AUTO',
  COOL = 'COOL',
  HEAT = 'HEAT',
  ECO = 'ECO',
  OFF = 'OFF',
  CUSTOM = 'CUSTOM',
}
// @formatter:on

export default ThermostatMode;
