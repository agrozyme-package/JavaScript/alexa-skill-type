import RequestName from '../Enumeration/RequestName';
import ResponseName from '../Enumeration/ResponseName';
import SkillNamespace from '../Enumeration/SkillNamespace';

export type HeaderName = RequestName | ResponseName;

export default interface Header {
  correlationToken?: string;
  messageId: string;
  name: HeaderName;
  namespace: SkillNamespace;
  payloadVersion: string;
}
