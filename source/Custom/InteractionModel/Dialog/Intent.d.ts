import Slot from './Slot';

export default interface Intent {
  confirmationRequired?: boolean;
  name: string;
  prompts?: { confirmation?: string; };
  slots?: Slot[];
}
