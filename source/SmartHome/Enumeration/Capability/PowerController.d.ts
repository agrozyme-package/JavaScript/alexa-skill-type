// @formatter:off
export const enum PowerControllerRequestName {
  TurnOn = 'TurnOn',
  TurnOff = 'TurnOff',
}

export const enum PowerControllerPropertyName {
  powerState = 'powerState'
}

// @formatter:on
