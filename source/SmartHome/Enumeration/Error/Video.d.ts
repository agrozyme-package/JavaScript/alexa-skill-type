/** @link https://developer.amazon.com/docs/device-apis/alexa-video-errorresponse.html#alexavideoerrorresponse */

// @formatter:off
declare const enum Video {
  NOT_SUBSCRIBED = 'NOT_SUBSCRIBED',
}
// @formatter:on

export default Video;
