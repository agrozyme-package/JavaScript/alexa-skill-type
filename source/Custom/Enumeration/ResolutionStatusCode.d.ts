/** @link https://developer.amazon.com/docs/custom-skills/request-types-reference.html#resolutions-object */

// @formatter:off
declare const enum ResolutionStatusCode {
  ER_SUCCESS_MATCH = 'ER_SUCCESS_MATCH',
  ER_SUCCESS_NO_MATCH = 'ER_SUCCESS_NO_MATCH',
  ER_ERROR_TIMEOUT = 'ER_ERROR_TIMEOUT',
  ER_ERROR_EXCEPTION = 'ER_ERROR_EXCEPTION',
}
// @formatter:on

export default ResolutionStatusCode;
