export default interface Value {
  id: string;
  name: { value: string; synonyms?: string[]; };
}
