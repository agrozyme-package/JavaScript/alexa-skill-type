// @formatter:off

export const enum TemperatureSensorPropertyName {
  temperature = 'temperature',
}

// @formatter:on
