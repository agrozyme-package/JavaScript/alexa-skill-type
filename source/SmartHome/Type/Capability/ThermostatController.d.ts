import Temperature from '../../Enumeration/Property/Temperature';
import ThermostatMode from '../../Enumeration/Property/ThermostatMode';
import Endpoint from '../Endpoint';
import Header from '../Header';
import Property from '../Property';

export interface ThermostatControllerRequest {
  directive: { header: Header; endpoint: Endpoint; payload: {}; };
}

export interface TemperatureValue {
  scale: Temperature;
  value: number;
}

export interface ThermostatControllerProperty extends Property {
  lowerSetpoint?: TemperatureValue,
  // temperature: TemperatureValue,
  targetSetpoint?: TemperatureValue,
  thermostatMode?: ThermostatMode
  upperSetpoint?: TemperatureValue,
}

export interface ThermostatControllerReponse {
  context: { properties: ThermostatControllerProperty[]; };
  event: { header: Header; endpoint: Endpoint; payload: {}; };
}
