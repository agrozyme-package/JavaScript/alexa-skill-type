/** @link https://developer.amazon.com/docs/custom-skills/request-and-response-json-reference.html#outputspeech-object */

// @formatter:off
declare const enum OutputSpeechType {
  PlainText = 'PlainText',
  SSML = 'SSML',
}
// @formatter:on

export default OutputSpeechType;
