// @formatter:off
declare const enum RequestName {
  Discover = 'Discover',
  TurnOn = 'TurnOn',
  TurnOff = 'TurnOff',
}
// @formatter:on

export default RequestName;
