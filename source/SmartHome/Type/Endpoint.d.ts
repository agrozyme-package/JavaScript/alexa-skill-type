import Scope from './Scope';

export default interface Endpoint {
  cookie: { [index: string]: string; };
  endpointId: string;
  scope: Scope;
}


