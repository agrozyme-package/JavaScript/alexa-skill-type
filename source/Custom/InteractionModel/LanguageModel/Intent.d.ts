import Slot from './Slot';

export default interface Intent {
  name: string;
  samples?: string[];
  slots?: Slot[];
}
