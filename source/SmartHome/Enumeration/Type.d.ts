// @formatter:off
declare const enum Type {
  FoodCount = 'FoodCount',
  Weight = 'Weight',
  Volume = 'Volume',
  EnumeratedPowerLevel = 'EnumeratedPowerLevel',
  IntegralPowerLevel = 'IntegralPowerLevel',
}
// @formatter:on

export default Type;
