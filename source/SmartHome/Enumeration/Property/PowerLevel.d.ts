/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#enumeratedpowerlevel */

// @formatter:off
declare const enum PowerLevel {
  LOW = 'LOW',
  MED_LOW = 'MED_LOW',
  MEDIUM = 'MEDIUM',
  MED_HIGH = 'MED_HIGH',
  HIGH = 'HIGH',
}
// @formatter:on

export default PowerLevel;
